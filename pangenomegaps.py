#!/usr/bin/env python3
import traceback

import argparse
from collections import defaultdict

from seqseqpan.io import Parser
from seqseqpan.exception import ConsensusFastaIdxFormatError, CoordinateOutOfBoundsError
from seqseqpan.mapper import Mapper


def get_pangenome_gaps(consensus_file):
    # parse index file
    parser = Parser()
    consensus_idx = parser.parse_consensus_index(consensus_file)

    # get alignment and info
    aln = consensus_idx[0]
    block_starts = consensus_idx[1].block_start_indices

    gap_array = defaultdict(list)
    
    # go through all lcbs in alignment
    for idx in range(len(aln.lcbs)):
        lcb = aln.lcbs[idx]
        block_start = block_starts[idx]

        # get entry per genome nr (turn around entries dict)
        entrypergenome = {entry.genome_nr: entry for entry in lcb.entries}

        # go through all genomes in alignment    
        for genome in aln.genomes:
            # if genome in entry find all gaps
            if genome in entrypergenome:
                for gap in [(start+block_start, end+block_start) for start,end in entrypergenome[genome].gaps.items()]:
                    gap_array[gap].append(genome)
            else:
                # genome is not part of entry - for this genome the whole entry is a gap in consensus genome
                gap = (block_start, block_start+lcb.length)
                gap_array[gap].append(genome)

    return gap_array

def print_pangenome_gaps(pangenome_name, pangenome_gaps, output_f):
    with open(output_f, 'w') as out:
        for pos in sorted(pangenome_gaps.keys()):
            genomes = pangenome_gaps[pos]
            for genome in genomes:
                # gaps in seq-seq-pan and bed files are stored with 0-based positions, inclusive start and exclusive end
                out.write('\t'.join([pangenome_name, str(pos[0]), str(pos[1]), "genome"+str(genome)+'\n']))


def get_pangenome_name(fasta_f):
    # name of pangenome is first line of consensus fasta
    with open(fasta_f) as fasta:
        name = fasta.readline().strip()[1:]
    return name


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Generate pangenome gaps .bed file. Make sure _consensus.fasta and _consensus.fasta.idx files are present in the same folder.")
    parser.add_argument("-c", "--consensus", dest="consensus_f", help= "Path and name of _consensus.fasta file (make sure _consensus.fasta.idx file is located in same folder and was not renamed).", required=True)
    parser.add_argument("-o", "--output", dest="pangenome_gaps_f", help="name of output file", required=True)

    args = parser.parse_args()    

    pangenome_name = get_pangenome_name(args.consensus_f)
    pangenome_gaps = get_pangenome_gaps(args.consensus_f)

    print_pangenome_gaps(pangenome_name, pangenome_gaps, args.pangenome_gaps_f)